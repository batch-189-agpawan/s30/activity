db.fruits.insertMany([
       {
           "name": "Banana",
           "supplier": "Farmer Fruits Inc.",
           "stocks": 30,
           "price": 20,
           "onSale": true
       },
       {
           "name": "Mango",
           "supplier": "Mango Magic Inc.",
           "stocks": 50,
           "price": 70,
           "onSale": true
       },
       {
           "name": "Dragon Fruit",
           "supplier": "Farmer Fruits Inc.",
           "stocks": 10,
           "price": 60,
           "onSale": true
       },
       {
           "name": "Grapes",
           "supplier": "Fruity Co.",
           "stocks": 30,
           "price": 100,
           "onSale": true
       },
       {
           "name": "Apple",
           "supplier": "Apple Valley",
           "stocks": 0,
           "price": 20,
           "onSale": false
       },
       {
           "name": "Papaya",
           "supplier": "Fruity Co.",
           "stocks": 15,
           "price": 60,
           "onSale": true
       }
]);


// count on sale :5
db.fruits.aggregate([
	{
		$match:{"onSale": true}
	},{
		$group: {_id: null, count: {$sum: 1} }
	}


	]);


// stocks more than 20
db.fruits.aggregate([
	{
		$match:{"stocks": { $gt : 20 }  }
	},
	{
		$group: {_id: "stockMoreThan20",  count: {$sum: 1} }
	}


	]);


// avg price on sale
db.fruits.aggregate([
	{	
		$match: {"onSale": true}
	},
	{	
		$group: {_id: "$supplier", avgAmount: {$avg: "$price"}}
	}

	]);

// max price of fruit per supplier
db.fruits.aggregate([
	{	
		$group: {_id: "$supplier", maxAmount: {$max: "$price"}}
	}

	]);

// min price of fruit
db.fruits.aggregate([
	{	
		$group: {_id: "$supplier", minAmount: {$min: "$price"}}
	}

	]);